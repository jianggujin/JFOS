/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos;

import java.util.List;

public interface JObjectListing {

    /**
     * 存储空间名称
     * 
     * @return
     */
    String getBucketName();

    /**
     * 下一次列举文件的起点
     */
    String getNextMarker();

    /**
     * 指明列举文件是否被截断。 列举完没有截断，返回值为false。 没列举完就有截断，返回值为true
     */
    boolean isTruncated();

    /**
     * 本次查询结果的前缀
     */
    String getPrefix();

    /**
     * 标明本次列举文件的起点
     */
    String getMarker();

    /**
     * 列举文件的最大个数
     */
    int getMaxKeys();

    /**
     * 对文件名称进行分组的一个字符
     */
    String getDelimiter();

    List<JObjectSummary> getObjectSummaries();
}
