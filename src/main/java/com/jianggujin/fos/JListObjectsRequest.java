/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos;

import lombok.Data;

@Data
public class JListObjectsRequest {
    public static final int DEFAULT_RETURNED_KEYS_LIMIT = 100;
    public static final int MAX_RETURNED_KEYS_LIMIT = 1000;
    /**
     * 存储空间名称
     */
    private String bucketName;
    /**
     * 限定返回的文件必须以prefix作为前缀
     */
    private String prefix;

    /**
     * 列举指定marker之后的文件
     */
    private String marker;

    /**
     * 限定此次列举文件的最大个数
     */
    private Integer maxKeys;

    /**
     * 对文件名称进行分组的一个字符。所有名称包含指定的前缀且第一次出现delimiter字符之间的文件作为一组元素（commonPrefixes）
     */
    private String delimiter;

    public JListObjectsRequest(String bucketName) {
        this.bucketName = bucketName;
        this.maxKeys = DEFAULT_RETURNED_KEYS_LIMIT;
    }

    public JListObjectsRequest(String bucketName, String prefix, String marker, Integer maxKeys, String delimiter) {
        this.bucketName = bucketName;
        this.prefix = prefix;
        this.marker = marker;
        this.delimiter = delimiter;
        if (maxKeys == null || maxKeys < 0) {
            this.maxKeys = DEFAULT_RETURNED_KEYS_LIMIT;
        } else if (maxKeys > MAX_RETURNED_KEYS_LIMIT) {
            this.maxKeys = MAX_RETURNED_KEYS_LIMIT;
        } else {
            this.maxKeys = maxKeys;
        }
    }

}
