/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.oss;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import com.aliyun.oss.common.auth.RequestSigner;
import com.aliyun.oss.common.comm.Protocol;
import com.aliyun.oss.common.comm.SignVersion;
import com.jianggujin.fos.JConfiguration;

import lombok.Data;
import lombok.NonNull;

@Data
public class JOSSConfiguration implements JConfiguration<JOSSClient> {

    @NonNull
    private String endPoint;
    @NonNull
    private String accessKeyId;
    @NonNull
    private String secretAccessKey;
    private JOSSClientConfiguration clientConfiguration;
    private JOSSSessionTokenConfiguration sessionTokenConfiguration;

    /**
     * https://help.aliyun.com/document_detail/100624.html?spm=a2c4g.11186623.2.10.431e412fClV5Px#concept-xzh-nzk-2gb
     * 
     * @author jianggujin
     *
     */
    @Data
    public static class JOSSSessionTokenConfiguration {
        private String securityToken;
        private String roleArn;
        private String roleSessionName;
        private String policy; // Optional
    }

    @Data
    public static class JOSSClientConfiguration {
        private String userAgent;
        private Integer maxErrorRetry;
        private Integer connectionRequestTimeout;
        private Integer connectionTimeout;
        private Integer socketTimeout;
        private Integer maxConnections;
        private Long connectionTTL;
        private Boolean useReaper;
        private Long idleConnectionTime;
        private Protocol protocol;
        private String proxyHost;
        private Integer proxyPort;
        private String proxyUsername;
        private String proxyPassword;
        private String proxyDomain;
        private String proxyWorkstation;
        private Boolean supportCname;
        private List<String> cnameExcludeList;
        private Lock rlock;
        private Boolean sldEnabled;
        private Integer requestTimeout;
        private Boolean requestTimeoutEnabled;
        private Long slowRequestsThreshold;
        private Map<String, String> defaultHeaders;
        private Boolean crcCheckEnabled;
        private List<RequestSigner> signerHandlers;
        private SignVersion signatureVersion;
    }
}