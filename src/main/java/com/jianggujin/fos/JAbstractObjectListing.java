/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos;

import java.util.Collections;
import java.util.List;

import lombok.ToString;

@ToString
public abstract class JAbstractObjectListing implements JObjectListing {

    private String bucketName;

    private String nextMarker;

    private boolean isTruncated;

    private String prefix;

    private String marker;

    private int maxKeys;

    private String delimiter;

    private List<JObjectSummary> objectSummaries = Collections.emptyList();

    public JAbstractObjectListing(String bucketName, String nextMarker, boolean isTruncated, String prefix,
            String marker, int maxKeys, String delimiter) {
        this.bucketName = bucketName;
        this.nextMarker = nextMarker;
        this.isTruncated = isTruncated;
        this.prefix = prefix;
        this.marker = marker;
        this.maxKeys = maxKeys;
        this.delimiter = delimiter;
    }

    public JAbstractObjectListing(String bucketName, String nextMarker, boolean isTruncated, String prefix,
            String marker, int maxKeys, String delimiter, List<JObjectSummary> objectSummaries) {
        this(bucketName, nextMarker, isTruncated, prefix, marker, maxKeys, delimiter);
        setObjectSummaries(objectSummaries);
    }

    @Override
    public String getBucketName() {
        return bucketName;
    }

    @Override
    public String getNextMarker() {
        return nextMarker;
    }

    @Override
    public boolean isTruncated() {
        return isTruncated;
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    @Override
    public String getMarker() {
        return marker;
    }

    @Override
    public int getMaxKeys() {
        return maxKeys;
    }

    @Override
    public String getDelimiter() {
        return delimiter;
    }

    @Override
    public List<JObjectSummary> getObjectSummaries() {
        return objectSummaries;
    }

    protected void setObjectSummaries(List<JObjectSummary> objectSummaries) {
        if (objectSummaries == null) {
            this.objectSummaries = Collections.emptyList();
        } else {
            this.objectSummaries = objectSummaries;
        }
    }
}
