/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.disk;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

import com.jianggujin.fos.JObjectMetadata;

import lombok.Getter;

@Getter
public class JDiskObjectMetadata implements JObjectMetadata {
    private File object;
    private Date lastModified;

    public JDiskObjectMetadata(File object) {
        this.object = object;
        this.lastModified = new Date(object.lastModified());
    }

    @Override
    public String getETag() {
        try {
            return URLEncoder.encode(object.getName(), "UTF-8") + object.length();
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    @Override
    public Date getLastModified() {
        return lastModified;
    }

    @Override
    public long getContentLength() {
        return object.length();
    }

    @Override
    public String getContentType() {
        return "application/stream";
    }
}
