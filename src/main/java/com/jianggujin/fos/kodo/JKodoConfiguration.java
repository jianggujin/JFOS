/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.kodo;

import java.util.Collections;
import java.util.Map;

import com.jianggujin.fos.JConfiguration;
import com.qiniu.http.Dns;
import com.qiniu.http.ProxyConfiguration;
import com.qiniu.storage.Region;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;

@Data
public class JKodoConfiguration implements JConfiguration<JKodoClient> {
    @NonNull
    private String accessKey;
    @NonNull
    private String secretKey;
    @NonNull
    private JRegion region;
    @NonNull
    private JHTTPType httpType = JHTTPType.HTTP;
    /**
     * 存储空间域名信息，用于生成访问地址，，格式为：www.example.com
     */
    @NonNull
    private Map<String, String> domainOfBucket = Collections.emptyMap();

    /**
     * 使用的Zone
     */
    @Deprecated
    private com.qiniu.common.Zone zone;

    /**
     * 空间相关上传管理操作是否使用 https , 默认 是
     */
    private Boolean useHttpsDomains;

    /**
     * 空间相关上传管理操作是否使用代理加速上传，默认 是
     */
    private Boolean accUpHostFirst;

    /**
     * 使用 AutoRegion 时，如果从区域信息得到上传 host 失败，使用默认的上传域名上传，默认 是 upload.qiniup.com,
     * upload-z1.qiniup.com, upload-z2.qiniup.com, upload-na0.qiniup.com,
     * upload-as0.qiniup.com
     */
    private Boolean useDefaultUpHostIfNone;

    /**
     * 如果文件大小大于此值则使用断点上传, 否则使用Form上传
     */
    private Integer putThreshold;

    /**
     * 连接超时时间 单位秒(默认10s)
     */
    private Integer connectTimeout;

    /**
     * 写超时时间 单位秒(默认 0 , 不超时)
     */
    private Integer writeTimeout;

    /**
     * 回复超时时间 单位秒(默认30s)
     */
    private Integer readTimeout;

    /**
     * 底层HTTP库所有的并发执行的请求数量
     */
    private Integer dispatcherMaxRequests;

    /**
     * 底层HTTP库对每个独立的Host进行并发请求的数量
     */
    private Integer dispatcherMaxRequestsPerHost;

    /**
     * 底层HTTP库中复用连接对象的最大空闲数量
     */
    private Integer connectionPoolMaxIdleCount;

    /**
     * 底层HTTP库中复用连接对象的回收周期（单位分钟）
     */
    private Integer connectionPoolMaxIdleMinutes;

    /**
     * 上传失败重试次数
     */
    private Integer retryMax;

    /**
     * 外部dns
     */
    private Dns dns;

    /*
     * 解析域名时,优先使用host配置,主要针对内部局域网配置
     */
    @Deprecated
    private Boolean useDnsHostFirst;

    /**
     * 代理对象
     */
    private ProxyConfiguration proxy;

    @AllArgsConstructor
    @Getter
    public enum JRegion {
        /**
         * 华东
         */
        HUADONG(Region.huadong(), "z0"),
        /**
         * 华北
         */
        HUABEI(Region.huabei(), "z1"),
        /**
         * 华南
         */
        HUANAN(Region.huanan(), "z2"),
        /**
         * 北美
         */
        BEUMEI(Region.beimei(), "na0"),
        /**
         * 东南亚
         */
        XINJIAPO(Region.xinjiapo(), "as0");
        private Region kodoRegion;
        private String region;
    }

    /**
     * http类型
     * 
     * @author jianggujin
     *
     */
    @AllArgsConstructor
    @Getter
    public enum JHTTPType {
        HTTP("http"), HTTPS("https");
        private String protocol;
    }
}