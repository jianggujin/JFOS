/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.kodo;

import java.util.ArrayList;
import java.util.List;

import com.jianggujin.fos.JAbstractObjectListing;
import com.jianggujin.fos.JObjectListing;
import com.jianggujin.fos.JObjectSummary;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.storage.model.FileListing;

public class JKodoObjectListing extends JAbstractObjectListing implements JObjectListing {

    public JKodoObjectListing(FileListing listing, String bucketName, String prefix, String marker, Integer maxKeys,
            String delimiter) {
        super(bucketName, listing.marker, listing.isEOF(), prefix, marker, maxKeys, delimiter);
        FileInfo[] infos = listing.items;
        if (infos != null && infos.length > 0) {
            List<JObjectSummary> objectSummaries = new ArrayList<JObjectSummary>(infos.length);
            for (FileInfo info : infos) {
                objectSummaries.add(new JKodoObjectSummary(bucketName, info));
            }
            setObjectSummaries(objectSummaries);
        }
    }
}