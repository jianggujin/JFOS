/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.kodo;

import java.util.Date;

import com.jianggujin.fos.JObjectSummary;
import com.qiniu.storage.model.FileInfo;

import lombok.Getter;

@Getter
public class JKodoObjectSummary implements JObjectSummary {
    private String bucketName;
    private Date lastModified;
    private FileInfo info;

    public JKodoObjectSummary(String bucketName, FileInfo info) {
        this.bucketName = bucketName;
        this.info = info;
        this.lastModified = new Date(info.putTime / 10000000);
    }

    @Override
    public String getETag() {
        return info.hash;// md5?
    }

    @Override
    public long getSize() {
        return info.fsize;
    }

    @Override
    public String getKey() {
        return info.key;
    }

}
