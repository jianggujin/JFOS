/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.bos;

import java.net.InetAddress;

import com.baidubce.Protocol;
import com.baidubce.Region;
import com.baidubce.http.RetryPolicy;
import com.jianggujin.fos.JConfiguration;

import lombok.Data;
import lombok.NonNull;

@Data
public class JBOSConfiguration implements JConfiguration<JBOSClient> {

    @NonNull
    private String accessKeyId;
    @NonNull
    private String secretAccessKey;
    private JBOSClientConfiguration clientConfiguration;
    private JBOSSessionTokenConfiguration sessionTokenConfiguration;

    /**
     * https://cloud.baidu.com/doc/BOS/s/Tjwvysda9/
     * 
     * @author jianggujin
     *
     */
    @Data
    public static class JBOSSessionTokenConfiguration {
        private String sessionToken;
        private Integer durationSeconds;
        private String acl;
    }

    @Data
    public static class JBOSClientConfiguration {
        private String endpoint;
        private String userAgent;
        private RetryPolicy retryPolicy;
        private InetAddress localAddress;
        private Protocol protocol;
        private String proxyHost;
        private Integer proxyPort;
        private String proxyUsername;
        private String proxyPassword;
        private String proxyDomain;
        private String proxyWorkstation;
        private Boolean proxyPreemptiveAuthenticationEnabled;
        private Integer maxConnections;
        private Integer socketTimeoutInMillis;
        private Integer connectionTimeoutInMillis;
        private Integer socketBufferSizeInBytes;
        private Region region;
        private Boolean cnameEnabled;
    }
}
