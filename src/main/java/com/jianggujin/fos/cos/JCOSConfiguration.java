/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.cos;

import com.jianggujin.fos.JConfiguration;
import com.qcloud.cos.endpoint.EndpointBuilder;
import com.qcloud.cos.endpoint.EndpointResolver;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.region.Region;

import lombok.Data;
import lombok.NonNull;

@Data
public class JCOSConfiguration implements JConfiguration<JCOSClient> {
    @NonNull
    private String secretId;
    @NonNull
    private String secretKey;

    private JCOSClientConfiguration clientConfiguration;
    private JCOSSessionTokenConfiguration sessionTokenConfiguration;

    /**
     * https://cloud.tencent.com/document/product/436/14048
     * 
     * @author jianggujin
     *
     */
    @Data
    public static class JCOSSessionTokenConfiguration {
        private String sessionToken;
        private Integer durationSeconds;
        private String bucket;
        private String region;
        private String allowPrefix;
        private String policy;
        private String[] allowActions;
    }

    @Data
    public static class JCOSClientConfiguration {
        /**
         * https://cloud.tencent.com/document/product/436/6224
         */
        @NonNull
        private Region region;

        private Integer connectionRequestTimeout;
        private Integer connectionTimeout;
        private Integer maxConnectionsCount;
        private String proxyPassword;
        private String proxyUsername;
        private Integer readLimit;
        private Integer signExpired;
        private Integer socketTimeout;
        private Boolean useBasicAuth;
        private String userAgent;
        private HttpProtocol httpProtocol;
        private String endPointSuffix;
        private EndpointBuilder endpointBuilder;
        private EndpointResolver endpointResolver;
        // http proxy代理，如果使用http proxy代理，需要设置IP与端口
        private String httpProxyIp = null;
        private Integer httpProxyPort = 0;
    }
}
