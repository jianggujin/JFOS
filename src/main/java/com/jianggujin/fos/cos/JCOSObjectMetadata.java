/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.cos;

import java.util.Date;

import com.jianggujin.fos.JObjectMetadata;
import com.qcloud.cos.model.ObjectMetadata;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class JCOSObjectMetadata implements JObjectMetadata {
    private ObjectMetadata objectMetadata;

    @Override
    public String getETag() {
        return objectMetadata.getETag();
    }

    @Override
    public Date getLastModified() {
        return objectMetadata.getLastModified();
    }

    @Override
    public long getContentLength() {
        return objectMetadata.getContentLength();
    }

    @Override
    public String getContentType() {
        return objectMetadata.getContentType();
    }
}
