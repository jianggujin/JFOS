/**
 * Copyright 2018-2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.cos;

import com.jianggujin.fos.JClientWrapper;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.COSCredentials;

import lombok.Getter;

/**
 * @author jianggujin
 *
 */
@Getter
public class JCOSClientWrapper implements JClientWrapper<COSClient> {
    private final COSClient client;

    public JCOSClientWrapper(COSCredentials credentials, ClientConfig config) {
        this.client = new COSClient(credentials, config);
    }

    @Override
    public void destory() {
        this.client.shutdown();
    }

}
