/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * 客户端
 * 
 * @author jianggujin
 *
 */
public interface JClient {
    /**
     * 列举存储空间
     * 
     * @return
     * @throws JFOSException
     */
    List<JBucket> listBuckets() throws JFOSException;

    /**
     * 创建存储空间
     * 
     * @param bucketName
     * @return
     * @throws JFOSException
     */
    JBucket createBucket(String bucketName) throws JFOSException;

    /**
     * 删除存储空间
     * 
     * @param bucketName
     * @throws JFOSException
     */
    void deleteBucket(String bucketName) throws JFOSException;

    /**
     * 判断存储空间是否存在
     * 
     * @param bucketName
     * @throws JFOSException
     */
    boolean doesBucketExist(String bucketName) throws JFOSException;

    /**
     * 列举文件
     * 
     * @param bucketName
     * @throws JFOSException
     */
    JObjectListing listObjects(String bucketName) throws JFOSException;

    /**
     * 列举文件
     * 
     * @param bucketName
     * @param prefix
     * @return
     * @throws JFOSException
     */
    JObjectListing listObjects(String bucketName, String prefix) throws JFOSException;

    /**
     * 列举文件
     * 
     * @param request
     * @throws JFOSException
     */
    JObjectListing listObjects(JListObjectsRequest request) throws JFOSException;

    /**
     * 获取对象属性
     * 
     * @param bucketName
     * @param key
     * @return
     * @throws JFOSException
     */
    JObjectMetadata getObjectMetadata(String bucketName, String key) throws JFOSException;

    /**
     * 删除对象
     * 
     * @param summary
     * @throws JFOSException
     */
    void deleteObject(JObjectSummary summary) throws JFOSException;

    /**
     * 删除对象
     * 
     * @param bucketName
     * @param key
     * @throws JFOSException
     */
    void deleteObject(String bucketName, String key) throws JFOSException;

    /**
     * 删除对象
     * 
     * @param bucketName
     * @param keys
     * @return 删除失败的列表
     * @throws JFOSException
     */
    List<String> deleteObjects(String bucketName, List<String> keys) throws JFOSException;

    /**
     * 复制对象
     * 
     * @param sourceBucketName
     * @param sourceKey
     * @param destinationBucketName
     * @param destinationKey
     * @throws JFOSException
     */
    void copyObject(String sourceBucketName, String sourceKey, String destinationBucketName, String destinationKey)
            throws JFOSException;

    /**
     * 流式下载对象
     * 
     * @param bucketName
     * @param key
     * @throws JFOSException
     */
    JObject getObject(String bucketName, String key) throws JFOSException;

    /**
     * 下载到本地文件
     * 
     * @param bucketName
     * @param key
     * @param file
     * @throws JFOSException
     */
    JObjectMetadata getObject(String bucketName, String key, File file) throws JFOSException;

    /**
     * 上传文件
     * 
     * @param bucketName
     * @param key
     * @param input
     * @throws JFOSException
     */
    void putObject(String bucketName, String key, InputStream input) throws JFOSException;

    /**
     * 上传文件
     * 
     * @param bucketName
     * @param key
     * @param input
     * @param contentType
     * @throws JFOSException
     */
    void putObject(String bucketName, String key, InputStream input, String contentType) throws JFOSException;

    /**
     * 上传文件
     * 
     * @param bucketName
     * @param key
     * @param file
     * @throws JFOSException
     */
    void putObject(String bucketName, String key, File file) throws JFOSException;

    /**
     * 上传文件
     * 
     * @param bucketName
     * @param key
     * @param file
     * @param contentType
     * @throws JFOSException
     */
    void putObject(String bucketName, String key, File file, String contentType) throws JFOSException;

    /**
     * 访问地址
     * 
     * @param bucketName
     * @param key
     * @param expires    失效时间，单位：秒
     * @return
     * @throws JFOSException
     */
    String generateUrl(String bucketName, String key, int expires) throws JFOSException;

    /**
     * 销毁客户端
     */
    void destory();
}
