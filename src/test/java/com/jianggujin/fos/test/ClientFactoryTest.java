package com.jianggujin.fos.test;

import java.util.List;

import com.jianggujin.fos.JClientFactory;
import com.jianggujin.fos.JFOSException;
import com.jianggujin.fos.JListObjectsRequest;
import com.jianggujin.fos.JObjectListing;
import com.jianggujin.fos.JObjectSummary;
import com.jianggujin.fos.kodo.JKodoClient;
import com.jianggujin.fos.kodo.JKodoConfiguration;
import com.jianggujin.fos.kodo.JKodoConfiguration.JRegion;

public class ClientFactoryTest {

    @org.junit.Test
    public void test() throws JFOSException {
//        File bucketRoot = new File("fos");
//        JDiskConfiguration configuration = new JDiskConfiguration(bucketRoot);
//        JDiskClient client = JClientFactory.newClient(configuration);
//        client.destory();

        JKodoConfiguration configuration = new JKodoConfiguration("GxZlIEPewgV8VzxlOExVNgH7JrVRZzodIqtmwsnP",
                "Eu4y63F_RLkz9FVuC41FqPyNvzag4dntOMvUolqN", JRegion.HUADONG);
        configuration.setConnectTimeout(60000);
        JKodoClient client = JClientFactory.newClient(configuration);
        JListObjectsRequest request = new JListObjectsRequest("wisedu-xyb");
        request.setMaxKeys(500);
        while (true) {
            JObjectListing listing = client.listObjects(request);
            if (listing == null) {
                break;
            }
            List<JObjectSummary> summaries = listing.getObjectSummaries();
            if (summaries == null || summaries.isEmpty()) {
                break;
            }

            for (JObjectSummary summary : summaries) {
                System.err.println(summary.getKey());
                client.deleteObject(summary.getBucketName(), summary.getKey());
            }
        }
        client.destory();
    }
}
