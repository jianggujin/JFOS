# 第一部分 简介
`JFOS`是用`Java` 编写的文件对象存储(`File Object Storage`)，对开发者提供标准的操作API实现文件对象的存储，支持阿里云OSS、腾讯云COS、华为云OBS、七牛云Kodo、百度云BOS、本地磁盘存储 。

# 第二部分 开始使用
使用`JFOS`可以直接下载源代码编译或者下载已经编译的`jar`文件，如果您是使用`maven`来构建项目，也可以直接在`pom.xml`中添加`JFOS`的坐标：

[![Maven central](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/JFOS/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/JFOS)

```xml
<!-- http://mvnrepository.com/artifact/com.jianggujin/JFOS -->
<dependency>
    <groupId>com.jianggujin</groupId>
    <artifactId>JFOS</artifactId>
    <version>最新版本</version>
</dependency>
```

最新的版本可以从[Maven仓库](http://mvnrepository.com/artifact/com.jianggujin/JFOS)或者[码云](https://gitee.com/jianggujin/JFOS)获取。

如果使用快照`SNAPSHOT`版本需要添加仓库，且版本号为快照版本 [点击查看最新快照版本号](https://oss.sonatype.org/content/repositories/snapshots/com/jianggujin/JFOS/)

```xml
<repository>
    <id>snapshots</id>
    <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
</repository>
```
